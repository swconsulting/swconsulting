<?php

namespace SAAL;

use Illuminate\Database\Eloquent\Model;

class Hora extends Model {

    protected $table = 'horas';

    public function aulas() {
        return $this->belongsToMany('SAAL\Aula', 'SAAL\HoraAula', 'hora_id', 'aula_id');
    }

    public function laboratorios() {
        return $this->belongsToMany('SAAL\Laboratorio', 'SAAL\HoraLaboratorio', 'hora_id', 'laboratorio_id');
    }

}
