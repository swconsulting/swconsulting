<?php

namespace SAAL;

use Illuminate\Database\Eloquent\Model;

class Aula extends Model {

    protected $table = 'aulas';

    public function horas() {
        return $this->belongsToMany('SAAL\Hora','App\HoraAula','aula_id','hora_id');
    }

}
