<?php

namespace SAAL;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model {

    protected $table = 'cursos';

    public function docentes() {
        return $this->belongsToMany('SAAL\Docente', 'SAAL\CursoDocente', 'curso_id', 'docente_id');
    }

}
