<?php

namespace SAAL;

use Illuminate\Database\Eloquent\Model;

class TipoSoftware extends Model {

    protected $table = 'tiposoftwares';

    public function softwares() {
        return $this->hasMany('SAAL\Software', 'tipoSoftware_id', 'id');
    }

}
