<?php

namespace SAAL;

use Illuminate\Database\Eloquent\Model;

class Laboratorio extends Model {

    protected $table = 'laboratorios';

    public function horas() {
        return $this->belongsToMany('SAAL\Hora', 'horalaboratorios', 'laboratorio_id', 'hora_id');
    }

    public function softwares() {
        return $this->belongsToMany('SAAL\Software', 'laboratoriosoftwares', 'laboratorio_id', 'software_id');
    }

}
