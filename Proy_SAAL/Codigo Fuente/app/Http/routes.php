<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
Route::post('nuevoLaboratorio', 'Laboratorio@create');
Route::get('laboratorio/update/{id}', 'Laboratorio@edit');
Route::post('laboratorio/update/{id}', 'Laboratorio@update');
Route::post('laboratorio/deleteLaboratorio', 'Laboratorio@destroy');
Route::get('search', 'Laboratorio@search');
Route::post('find', 'Laboratorio@find');
Route::get('/', 'WelcomeController@index');
Route::get('{id}', 'WelcomeController@index2');


