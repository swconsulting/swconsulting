<?php

namespace SAAL\Http\Controllers;
use \SAAL\Laboratorio as Laboratorio;
class WelcomeController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Welcome Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index() {
        $lab = \SAAL\Laboratorio::first();
        return redirect('/' . $lab->id);
    }
	
	/**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index2($id) {
        $laboratorios = Laboratorio::all()->toArray();
        $hora = \SAAL\Hora::all()->toArray();
        $lab = Laboratorio::find($id);
        $hs = \SAAL\HoraLaboratorio::where('laboratorio_id','=',$lab->id)->get();
        return view('welcome', compact('laboratorios', 'hora', 'lab','hs'));
    }

}
