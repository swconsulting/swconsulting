<?php

namespace SAAL\Http\Controllers;

use SAAL\Http\Requests;
use SAAL\Http\Controllers\Controller;
use \SAAL\Http\Requests\CreateLab;
use \SAAL\Http\Requests\UpdateLab;
use \SAAL\Http\Requests\DeleteLab;
use \SAAL\Http\Requests\SearchReq;
use Illuminate\Http\Request;
use \SAAL\Laboratorio as Lab;
use \SAAL\Hora as Hora;
use Illuminate\Support\Facades\DB;
class Laboratorio extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(CreateLab $request) {
        $lab = new Lab;
        $lab->numero = $request->codigo;
        $lab->capacidad = $request->numequipo;
        $lab->proyector = ($request->proyector) ? 1 : 0;
        $lab->save();
        $hora = Hora::all()->toArray();
        $hs = array();
        foreach ($hora as $h) {
            $hs[] = $h['id'];
        }
        $lab->horas()->sync($hs);
        return $lab->id;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $lab = Lab::find($id);
        if (is_null($lab)) {
            return redirect()->back();
        }
        $laboratorios = \SAAL\Laboratorio::all()->toArray();
        $items = \SAAL\Software::all();
        $labSoft = $lab->softwares->toArray();
        $soft = array();
        foreach ($labSoft as $s) {
            $soft[] = $s['id'];
        }
        return view('codigo', compact('laboratorios', 'lab', 'items', 'soft'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $request
     * @return Response
     */
    public function update(UpdateLab $request) {
        try {
            $lab = Lab::find($request->id);
            $lab->capacidad = $request->numero;
            $lab->proyector = ($request->proyector) ? 1 : 0;
            $lab->save();
            if ($request->option != 'null') {
                $lab->softwares()->sync(explode(",", $request->option));
            } else {
                $soft = $lab->softwares->toArray();
                $arr = array();
                foreach ($soft as $s) {
                    array_push($arr, $s['id']);
                }
                $lab->softwares()->detach($arr);
            }
        } catch (Exception $ex) {
            return 'false';
        }
        return 'true';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(DeleteLab $request) {
        $lab = Lab::find($request->codigoDelete);
        if (is_null($lab)) {
            return "home";
        }
        $soft = $lab->softwares->toArray();
        $arr = array();
        foreach ($soft as $s) {
            array_push($arr, $s['id']);
        }
        $lab->softwares()->detach($arr);
        $lab->delete();
        return "ok";
    }

    public function search() {
        $laboratorios = Lab::all()->toArray();
        $labs = array();
        return view('search', compact('labs', 'laboratorios'));
    }

    public function find(SearchReq $request) {
        $laboratorios = Lab::all()->toArray();
        $lab = DB::table('laboratorios');
        if ($request->inputCodigo != "")
            $lab = $lab->where('numero', '=', $request->inputCodigo);
        if ($request->inputNumero != "")
            $lab = $lab->where('capacidad', '=', $request->inputNumero);
        $flag = 0;
        if (!is_null($request->inputProyector))
            $flag = 1;
        
        $labs = $lab->where('proyector', '=', $flag)->get();
        return view('search', compact('labs', 'laboratorios'));
    }

}
