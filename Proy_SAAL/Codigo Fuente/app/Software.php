<?php

namespace SAAL;

use Illuminate\Database\Eloquent\Model;

class Software extends Model {

    protected $table = 'softwares';

    public function tipoSoftware() {
        return $this->belongsTo('SAAL\TipoSoftware', 'id', 'tipoSoftware_id');
    }

    public function laboratorios() {
        return $this->belongsToMany('SAAL\Laboratorio','laboratoriosoftwares', 'software_id', 'laboratorio_id');
    }

}
