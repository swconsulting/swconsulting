<?php

namespace SAAL;

use Illuminate\Database\Eloquent\Model;

class Docente extends Model {

    protected $table = 'docentes';

    public function cursos() {
        return $this->belongsToMany('SAAL\Curso', 'SAAL\CursoDocente', 'docente_id', 'curso_id');
    }

}
