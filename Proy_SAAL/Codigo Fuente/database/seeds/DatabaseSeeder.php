<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use \SAAL\Laboratorio as Laboratorio;
use \SAAL\Hora as Hora;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();

        $this->call('UserTableSeeder');
        $this->call('TipoSoftwareTableSeeder');
        $this->call('SoftwareTableSeeder');
        $this->call('CursosTableSeeder');
        $this->call('DocenteTableSeeder');
        $this->call('CursoDocenteTableSeeder');
        $this->call('LaboratorioTableSeeder');
        $this->call('HoraTableSeeder');
    }

}

class UserTableSeeder extends Seeder {

    public function run() {
        DB::table('users')->delete();
        SAAL\User::create(['email' => 'admin@admin.com', 'clave' => \Illuminate\Support\Facades\Hash::make('123456')]);
    }

}

class TipoSoftwareTableSeeder extends Seeder {

    public function run() {
        DB::table('tipoSoftwares')->delete();
        SAAL\TipoSoftware::create(['nombre' => 'IDE']);
        SAAL\TipoSoftware::create(['nombre' => 'OFIMATICA']);
        SAAL\TipoSoftware::create(['nombre' => 'SGBD']);
    }

}

class SoftwareTableSeeder extends Seeder {

    public function run() {
        DB::table('softwares')->delete();
        $user = SAAL\User::where('email', '=', 'admin@admin.com')->first();
        $tipoSoftware = SAAL\TipoSoftware::where('nombre', '=', 'SGBD')->first();
        SAAL\Software::create(['nombre' => 'MySql', 'company' => 'Oracle Corporation and/or its affiliates', 'version' => '5.6', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
        SAAL\Software::create(['nombre' => 'Oracle', 'company' => 'Oracle Corporation and/or its affiliates', 'version' => '5.6', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
        SAAL\Software::create(['nombre' => 'Paradox', 'company' => 'Paradox', 'version' => '5.6', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
        SAAL\Software::create(['nombre' => 'Microsoft SQL Server', 'company' => 'Microsoft', 'version' => '5.6', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
        SAAL\Software::create(['nombre' => 'Microsoft Access', 'company' => 'Microsoft', 'version' => '5.6', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
        SAAL\Software::create(['nombre' => 'DB2', 'company' => 'DB2', 'version' => '5.6', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
        SAAL\Software::create(['nombre' => 'Visual FoxPro', 'company' => 'FoxPro', 'version' => '5.6', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);

        $tipoSoftware = SAAL\TipoSoftware::where('nombre', '=', 'IDE')->first();
        SAAL\Software::create(['nombre' => 'Eclipse', 'company' => 'Eclipse', 'version' => '2.2', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
        SAAL\Software::create(['nombre' => 'NetBeans', 'company' => 'NetBeans', 'version' => '2.2', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
        SAAL\Software::create(['nombre' => 'Jcreator', 'company' => 'Jcreator', 'version' => '2.2', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
        SAAL\Software::create(['nombre' => 'CodeBlocks', 'company' => 'CodeBlocks', 'version' => '2.2', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
        SAAL\Software::create(['nombre' => 'DevC++', 'company' => 'DevC++', 'version' => '2.2', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);

        $tipoSoftware = SAAL\TipoSoftware::where('nombre', '=', 'OFIMATICA')->first();
        SAAL\Software::create(['nombre' => 'Word', 'company' => 'Microsoft', 'version' => '2.2', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
        SAAL\Software::create(['nombre' => 'Excel', 'company' => 'Microsoft', 'version' => '2.2', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
        SAAL\Software::create(['nombre' => 'PowerPoint', 'company' => 'Microsoft', 'version' => '2.2', 'tipoSoftware_id' => $tipoSoftware->id, 'user_id' => $user->id]);
    }

}

class CursosTableSeeder extends Seeder {

    public function run() {
        DB::table('cursos')->delete();
        SAAL\Curso::create(['nombre' => 'Comunicación y Dinámica de Grupo', 'ciclo' => '1', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Matematica Básica I', 'ciclo' => '1', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Calculo I', 'ciclo' => '1', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Teoria General de Sistemas', 'ciclo' => '1', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Introducción a la Computación', 'ciclo' => '1', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Estrategias de Aprendizaje e Investigación', 'ciclo' => '1', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Ética de la Profesión', 'ciclo' => '1', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Algoritmica I', 'ciclo' => '2', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Creatividad y Liderazgo', 'ciclo' => '2', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Calculo II', 'ciclo' => '2', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Matematica Básica II', 'ciclo' => '2', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Fisica General I', 'ciclo' => '2', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Economia', 'ciclo' => '2', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Realidad Nacional', 'ciclo' => '2', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Algoritmica II', 'ciclo' => '3', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Series y Ecuaciones Diferenciales', 'ciclo' => '3', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Estadistica I', 'ciclo' => '3', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Fisica General II', 'ciclo' => '3', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Matemática Discreta', 'ciclo' => '3', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Organización y Administración', 'ciclo' => '3', 'escuela' => '1']);
        SAAL\Curso::create(['nombre' => 'Introducción a la Teoria General de Sistemas', 'ciclo' => '1', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Introducción a la Computación e Ingeniería de Software', 'ciclo' => '1', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Cálculo I', 'ciclo' => '1', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Matemática Básica I', 'ciclo' => '1', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Comunicación y Dinámica de Grupo', 'ciclo' => '1', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Idioma Extranjero I', 'ciclo' => '1', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Programación I', 'ciclo' => '2', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Matemáticas Discretas', 'ciclo' => '2', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Calculo II', 'ciclo' => '2', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Matemática Básica II', 'ciclo' => '2', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Fisica I', 'ciclo' => '2', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Idioma Extranjero II', 'ciclo' => '2', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Programación II', 'ciclo' => '3', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Series y Ecuaciones Diferenciales', 'ciclo' => '3', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Estadisticas y Probabilidades', 'ciclo' => '3', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Física II', 'ciclo' => '3', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Organización y Administración', 'ciclo' => '3', 'escuela' => '2']);
        SAAL\Curso::create(['nombre' => 'Idioma Extranjero III', 'ciclo' => '3', 'escuela' => '2']);
    }

}

class DocenteTableSeeder extends Seeder {

    public function run() {
        DB::table('docentes')->delete();
        SAAL\Docente::create(['nombre' => 'Chavez Soto, Jorge']);
        SAAL\Docente::create(['nombre' => 'Yañez Duran, Carlos Enrique']);
        SAAL\Docente::create(['nombre' => 'Leon Fernandez, Cayo Víctor']);
        SAAL\Docente::create(['nombre' => 'Alarcon Loayza, Luis']);
        SAAL\Docente::create(['nombre' => 'Alcantara Loayza, César Augusto']);
        SAAL\Docente::create(['nombre' => 'Alva Bravo, Alfredo']);
        SAAL\Docente::create(['nombre' => 'Angulo Calderón, César']);
        SAAL\Docente::create(['nombre' => 'Armas Calderon, Raúl']);
        SAAL\Docente::create(['nombre' => 'Avendaño Quiroz, Johnny Robert']);
        SAAL\Docente::create(['nombre' => 'Bustamante Olivera, Victor Hugo']);
        SAAL\Docente::create(['nombre' => 'Cabrera Diaz, Javier']);
        SAAL\Docente::create(['nombre' => 'Calmet Agnelli, Roberto Francisco']);
        SAAL\Docente::create(['nombre' => 'Canepa Perez, Carlos']);
        SAAL\Docente::create(['nombre' => 'Carrasco Ore, Nilo Eloy']);
        SAAL\Docente::create(['nombre' => 'Castro Leon, Gloria Helena']);
        SAAL\Docente::create(['nombre' => 'CHÁVEZ HERRERA, Carlos Ernesto']);
        SAAL\Docente::create(['nombre' => 'Contreras Flores, Walter']);
    }

}

class CursoDocenteTableSeeder extends Seeder {

    public function run() {
        DB::table('cursodocentes')->delete();
        $docente = SAAL\Docente::all()->toArray();
        $curso = SAAL\Curso::all()->toArray();
        $user = SAAL\User::where('email', '=', 'admin@admin.com')->first();
        for ($i = 0; $i < 20; $i++) {
            $d = $docente[rand(0, count($docente) - 1)];
            $c = $curso[rand(0, count($curso) - 1)];
            SAAL\CursoDocente::create(['tipo' => rand(1, 2), 'horas' => rand(1, 4), 'docente_id' => $d['id'], 'curso_id' => $c['id'], 'user_id' => $user->id]);
        }
    }

}

class LaboratorioTableSeeder extends Seeder {

    public function run() {
        DB::table('laboratorios')->delete();
        for ($i = 0; $i < 10; $i++) {
            SAAL\Laboratorio::create(['numero' => $i + 1, 'capacidad' => rand(15, 25), 'proyector' => rand(0, 1)]);
        }
    }

}

class HoraTableSeeder extends Seeder {

    public function run() {
        DB::table('horas')->delete();
        $dia = ['lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado'];
        foreach ($dia as $d) {
            for ($i = 8; $i < 22;) {
                SAAL\Hora::create([
                    'inicio' => str_pad($i, 2, '0', STR_PAD_LEFT) . ':00',
                    'fin' => str_pad(++$i, 2, '0', STR_PAD_LEFT) . ':00',
                    'dia' => $d
                ]);
            }
        }
    }

}
