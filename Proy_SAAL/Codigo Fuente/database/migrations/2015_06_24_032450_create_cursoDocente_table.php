<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursoDocenteTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('cursoDocentes', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('tipo', ['1', '2']);
            $table->tinyInteger('horas');

            $table->integer('docente_id')->unsigned();
            $table->integer('curso_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('docente_id')->references('id')->on('docentes');
            $table->foreign('curso_id')->references('id')->on('cursos');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('cursoDocentes');
    }

}
