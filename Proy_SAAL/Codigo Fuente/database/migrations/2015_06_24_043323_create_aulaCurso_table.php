<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAulaCursoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('aulaCursos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cursoDocente_id')->unsigned();
            $table->integer('users_id')->unsigned();

            $table->foreign('cursoDocente_id')->references('id')->on('cursoDocentes');
            $table->foreign('users_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('aulaCursos');
    }

}
