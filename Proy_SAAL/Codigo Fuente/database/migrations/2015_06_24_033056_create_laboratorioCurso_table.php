<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaboratorioCursoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('laboratorioCursos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cursoDocente_id')->unsigned();

            $table->foreign('cursoDocente_id')->references('id')->on('cursoDocentes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('laboratorioCursos');
    }

}
