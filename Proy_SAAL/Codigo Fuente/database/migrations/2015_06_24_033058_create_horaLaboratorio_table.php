<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoraLaboratorioTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('horaLaboratorios', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('disponible');

            $table->integer('laboratorio_id')->unsigned();
            $table->integer('hora_id')->unsigned();
           

            $table->foreign('laboratorio_id')->references('id')->on('laboratorios');
            $table->foreign('hora_id')->references('id')->on('horas');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('horaLaboratorios');
    }

}
