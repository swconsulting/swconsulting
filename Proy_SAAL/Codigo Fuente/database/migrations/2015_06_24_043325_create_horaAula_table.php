<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoraAulaTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('horaAulas', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('disponible');

            $table->integer('hora_id')->unsigned();
            $table->integer('aula_id')->unsigned();
            $table->integer('aulaCurso_id')->unsigned();

            $table->foreign('hora_id')->references('id')->on('horas');
            $table->foreign('aula_id')->references('id')->on('aulas');
            $table->foreign('aulaCurso_id')->references('id')->on('aulaCursos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('horaAulas');
    }

}
