<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaboratorioSoftwareTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('laboratorioSoftwares', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('laboratorio_id')->unsigned();
            $table->integer('software_id')->unsigned();

            $table->foreign('laboratorio_id')->references('id')->on('laboratorios');
            $table->foreign('software_id')->references('id')->on('softwares');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('laboratorioSoftwares');
    }

}
