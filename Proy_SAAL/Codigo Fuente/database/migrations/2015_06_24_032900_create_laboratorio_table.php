<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaboratorioTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('laboratorios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero');
            $table->integer('capacidad');
            $table->tinyInteger('proyector');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('laboratorios');
    }

}
