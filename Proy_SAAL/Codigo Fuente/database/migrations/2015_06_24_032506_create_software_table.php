<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftwareTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('softwares', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('company');
            $table->string('version');
            
            $table->integer('tipoSoftware_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('tipoSoftware_id')->references('id')->on('tipoSoftwares');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('softwares');
    }

}
