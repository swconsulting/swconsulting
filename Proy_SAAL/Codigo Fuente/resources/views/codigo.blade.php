<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Dashboard Template for Bootstrap</title>
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/dashboard.css')}}" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <link href="{{asset('css/bootstrap-duallistbox.css')}}" rel="stylesheet">
        <script src="{{asset('js/jquery.bootstrap-duallistbox.js')}}"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Pedro</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Mantenimiento de Laboratorio  <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" data-toggle="modal" data-target="#nuevoLaboratorioModal">Nuevo Laboratorio</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#editarLaboratorioModal">Editar Laboratorio</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#eliminarLaboratorioModal">Eliminar Laboratorio</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{URL::to('search')}}">Buscar Laboratorio</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Nuevo Item</a></li>
                                <li><a href="#">Editar Item</a></li>
                                <li><a href="#">Eliminar Item</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Asignación de Laboratorio </a></li>
                        <li><a href="#">Reporte </a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">pedro@smartec.la <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Salir</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        @foreach ( $laboratorios as $l )
                        <li><a href="#">Laboratorio {{str_pad($l['numero'], 2, "0", STR_PAD_LEFT)}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <div class="col-md-5">
                        <form id="LabUpdate" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $lab->id }}">
                            <div class="form-group">
                                <label for="inputCodigo" class="col-sm-4 control-label">Código</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="codigo" name="codigo" placeholder="Código" value="{{$lab->numero}}" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputNumero" class="col-sm-4 control-label">Número de equipos</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="numero" name="numero" placeholder="Número de equipos" value="{{$lab->capacidad}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="proyector" id="proyector" @if($lab->proyector == 1) checked @endif> Proyector
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="button" id="cancel" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" id="updateLaboratorio" class="btn btn-primary">Guardar</button>
                                </div>
                            </div>
                        </form>
                        <script>
$('#cancel').on("click", function () {
    location.href = "{{URL::to('/')}}";
});
$('#LabUpdate').submit(function () {
    var $form = $('#LabUpdate').serialize();
    var $select = $("#duallistbox_demo2").val();
    $data = $form + '&option=' + $select;
    $.ajax({
        url: '{{URL::to("laboratorio/update/$lab->id")}}',
        data: $data,
        method: "POST",
    })
            .done(function (data) {
                if (data == 'true') {
                    location.href = "{{URL::to('/')}}";
                } else {
                    alert('Error! Vuelva a intentarlo');
                }
            });
    return false;
});
/*$("#updateLaboratorio").on("click", function () {
 var $form = $('#LabUpdate').serialize();
 console.log($form);
 });*/
                        </script>
                    </div>
                    <div class="col-md-7">
                        <select multiple="multiple" size="10" name="duallistbox_demo2" id="duallistbox_demo2" class="demo2">
                            @foreach($items as $it)
                            <option value="{{$it->id}}" 
                                    @if(in_array($it->id, $soft)) selected @endif 
                                    >{{$it->nombre}}</option>
                            @endforeach
                        </select>
                        <script>
                            var demo2 = $('.demo2').bootstrapDualListbox({
                                nonSelectedListLabel: 'Softwares',
                                selectedListLabel: 'Instalados',
                                preserveSelectionOnMove: 'moved',
                                moveOnSelect: false,
                                //nonSelectedFilter: 'ion ([7-9]|[1][0-2])'
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
        <script src="../../assets/js/vendor/holder.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>


        <!-- Modal Nuevo Laboratorio -->
        <div class="modal fade" id="nuevoLaboratorioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Nevo Laboratorio</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Codigo:</label>
                                <input type="text" class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Número de equipos:</label>
                                <input type="text" class="form-control" id="recipient-name">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Proyector
                                </label>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary">Crear</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Editar Laboratorio -->
        <div class="modal fade" id="editarLaboratorioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Editar Laboratorio</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Codigo:</label>
                                <input type="text" class="form-control" id="recipient-name">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary">Editar</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal Eliminar Laboratorio -->
        <div class="modal fade" id="eliminarLaboratorioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Eliminar Laboratorio</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Codigo:</label>
                                <input type="text" class="form-control" id="recipient-name">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
