<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Dashboard Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/dashboard.css" rel="stylesheet">

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

        <!-- Custom styles for duallisbox -->
        <link href="css/bootstrap-duallistbox.css" rel="stylesheet">
        <script src="js/jquery.bootstrap-duallistbox.js"></script>

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <!--<script src="../../assets/js/ie-emulation-modes-warning.js"></script>-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>


        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Pedro</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Mantenimiento de Laboratorio  <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" data-toggle="modal" data-target="#nuevoLaboratorioModal">Nuevo Laboratorio</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#editarLaboratorioModal">Editar Laboratorio</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#eliminarLaboratorioModal">Eliminar Laboratorio</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{URL::to('search')}}">Buscar Laboratorio</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Nuevo Item</a></li>
                                <li><a href="#">Editar Item</a></li>
                                <li><a href="#">Eliminar Item</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Asignación de Laboratorio </a></li>
                        <li><a href="#">Reporte </a></li>  
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">pedro@smartec.la <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Salir</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        @foreach ( $laboratorios as $l )
                        <li><a href="#">Laboratorio {{$l['numero']}}</a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

                    <form class="form-horizontal" action="{{URL::to('find')}}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div style="display: inline-block">
                            <input type="text" class="form-control" name="inputCodigo" id="inputCodigo" placeholder="Código">
                        </div>
                        <div style="display: inline-block">
                            <input type="text" class="form-control" name="inputNumero" id="inputNumero" placeholder="Número de equipos">
                        </div>
                        <div style="display: inline-block">
                            <div class="checkbox" style="display: inline-block">
                                <label>
                                    <input type="checkbox" name="inputProyector" id="inputProyector"> Proyecto
                                </label>
                            </div>
                        </div>
                        <div style="display: inline-block">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                            <button type="button" class="btn btn-default">Cancelar</button>
                        </div>
                    </form>
                    <table class="table">
                        <caption>Optional table caption.</caption>
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Capacidad</th>
                                <th>Proyector</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($labs as $lab)
                            <tr>
                                <th>Laboratorio {{ $lab->numero}}</th>
                                <td>{{ $lab->capacidad}}</td>
                                <td>{{ $lab->proyector}}</td>
                                <td><a type="submit" href="{{URL::to('laboratorio/update/'.$lab->id)}}" class="btn btn-primary">Editar</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
        <script src="../../assets/js/vendor/holder.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>
