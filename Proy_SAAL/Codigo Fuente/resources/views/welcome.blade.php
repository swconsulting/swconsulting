<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Dashboard Template for Bootstrap</title>
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/dashboard.css')}}" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Pedro</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Mantenimiento de Laboratorio  <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" data-toggle="modal" data-target="#nuevoLaboratorioModal">Nuevo Laboratorio</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#editarLaboratorioModal">Editar Laboratorio</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#eliminarLaboratorioModal">Eliminar Laboratorio</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{URL::to('search')}}">Buscar Laboratorio</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#" data-toggle="modal" data-target="#nuevoItemModal">Nuevo Item</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#editarItemModal">Editar Item</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#eliminarItemModal">Eliminar Item</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Asignación de Laboratorio </a></li>
                        <li><a href="#">Reporte </a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <!--<li><a href="#">Link</a></li>-->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">pedro@smartec.la <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Salir</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <!--li class="active"><a href="#">Lab01</a></li-->
                        @foreach ( $laboratorios as $l )
                        <li><a href="#">Laboratorio {{$l['numero']}}</a></li>
                        @endforeach
                    </ul>
                </div>


                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">Horario</h1>
                    <!--<h2 class="sub-header">Section title</h2>-->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Horario</th>
                                    <th>Lunes</th>
                                    <th>Martes</th>
                                    <th>Miercoles</th>
                                    <th>Jueves</th>
                                    <th>Viernes</th>
                                    <th>Sabado</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
        <script src="../../assets/js/vendor/holder.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>



        <!-- Modal Nuevo Laboratorio -->
        <div class="modal fade" id="nuevoLaboratorioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Nuevo Laboratorio</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form1">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Codigo:</label>
                                <input type="text" class="form-control" id="codigo" name="codigo">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Número de equipos:</label>
                                <input type="text" class="form-control" id="numequipo" name="numequipo">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="proyector" name="proyector"> Proyector
                                </label>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button id="crearLaboratorio" type="button" class="btn btn-primary">Crear</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
$("#crearLaboratorio").on("click", function () {
    var $form = $('#form1').serialize();
    $.ajax({
        url: "nuevoLaboratorio",
        data: $form,
        method: "POST",
    })
            .done(function (data) {
                location.href = "{{URL::to('laboratorio/update')}}/" + data;
            });
});</script>
        <!-- Modal Editar Laboratorio -->
        <div class="modal fade" id="editarLaboratorioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Editar Laboratorio</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Codigo:</label>
                            <input type="text" class="form-control" id="codigoEdit" name="codigoEdit">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="editLaboratorio">Editar</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $("#editLaboratorio").on("click", function () {
                var $codigoEdit = $('#codigoEdit').val();
                location.href = "laboratorio/update/" + $codigoEdit;
            });
                </script>

        <!-- Modal Eliminar Laboratorio -->
        <div class="modal fade" id="eliminarLaboratorioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Eliminar Laboratorio</h4>
                    </div>
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Codigo:</label>
                            <input type="text" class="form-control" id="codigoDelete" name="codigoDelete">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="deleteLaboratio">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $("#deleteLaboratio").on("click", function () {
                var $codigoDelete = $('#codigoDelete').val();
                var $_token = $('#_token').val();
                $.ajax({
                    url: "laboratorio/deleteLaboratorio",
                    data: {codigoDelete: $codigoDelete, _token: $_token},
                    method: "POST",
                })
                        .done(function (data) {
                            if (data == 'home') {
                                alert('El laboratorio no existe');
                            } else if ('ok') {
                                alert('El laboratorio fue eliminado');
                            }
                            location.href = "{{URL::to('/')}}";
                        }).fail(function () {
                    alert("error");
                })
            });
        </script>


    </body>
</html>
